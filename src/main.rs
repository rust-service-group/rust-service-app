use actix_web::{web, App, HttpRequest, HttpServer, Responder};
use listenfd::ListenFd;

async fn greet(req: HttpRequest) -> impl Responder {
    let name = req.match_info().get("name").unwrap_or("World");
    format!("Hello again {}!", &name)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let mut listenfd = ListenFd::from_env();
    let mut server = HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
            .route("/{name}", web::get().to(greet))
    });
    server = if let Some(listener) = listenfd.take_tcp_listener(0)? {
        println!("found fd socket, listening on it");
        server.listen(listener)?
    // otherwise fall back to local listening
    } else {
        println!("Cannot find fd socket, listening on 0.0.0.0:5000");
        server.bind("0.0.0.0:5000")?
    };
    println!("running server ...");
    server.run().await
}
